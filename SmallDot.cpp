//Include our classes
#include "SmallDot.h"
#include "Scene.h"

SmallDot::SmallDot() {
	mpAlive = true;
	setXY(0, 0);
	setW(TILE_SIZE / 4);
	setH(TILE_SIZE / 4);		//Inicializar en alg�n sitio
	
	mpColor = ofColor(0, 0, 0);

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpGraphicImg = new ofImage();
	mpGraphicImg->load("SmallDot.png");

	mFrame = 0;
	mCurrentFrameTime = 0;
}

SmallDot::SmallDot(int x, int y, int h, int w){
	setXY(x, y);
	setW(w);
	setH(h);
}

void SmallDot::init() {
	mpAlive = true;
}

bool SmallDot::getAlive() { 
	return mpAlive; 
}
void SmallDot::setAlive(bool alive) {
	mpAlive = alive;
}


SmallDot::~SmallDot() {//Solo tiene que tener cosas si hay atributos que canvien de tama�o, los atributos constantes no.

}

void SmallDot::update() {
	if (!mpAlive) {return;}
}


void SmallDot::render() {
	if (!mpAlive) { return; }
	ofSetColor(255, 255, 255);
	mpGraphicImg->drawSubsection(mpRect.x - 10, mpRect.y - 10,
								 mpGraphicRect.w, mpGraphicRect.h,
								 mpGraphicRect.x, mpGraphicRect.y);
}


//Setters and getters
void SmallDot::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setW(a_rect.h);
	return;
}

void SmallDot::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void SmallDot::setX(int x) {
	mpRect.x = x;
	return;
}

void SmallDot::setY(int y) {
	mpRect.y = y;
	return;
}
void SmallDot::setW(int w) {
	mpRect.w = w;
	return;
}

void SmallDot::setH(int h) {
	mpRect.h = h;
	return;
}


//Gets
C_Rectangle SmallDot::getRect() {

	return mpRect;
}
int SmallDot::getX() {

	return mpRect.x;
}
int SmallDot::getY() {

	return mpRect.y;
}
int SmallDot::getW() {

	return mpRect.w;
}
int SmallDot::getH() {

	return mpRect.h;
}

void SmallDot::setColor(ofColor color){
	mpColor = color;
	return;
}