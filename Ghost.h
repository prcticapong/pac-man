#ifndef Ghost_H
#define Ghost_H

#include "includes.h"
#include "ofMain.h"
#include "Utils.h"

//enum ghostDirections { NONE, UP, DOWN, LEFT, RIGHT };

class Ghost {

public:
	Ghost();
	Ghost(int x, int y, int w, int h);
	~Ghost();

	void init();
	bool getAlive();
	void setAlive(bool alive);
	void update();
	void render();


	//Setters and Getters
	void setRect(C_Rectangle a_rect);
	void setXY(int x, int y);

	void setX(int x);
	void setY(int y);
	void setW(int w);
	void setH(int h);


	C_Rectangle getRect();
	int getX();
	int getY();
	int getW();
	int getH();


	void setSpeed(int speed);
	int getSpeed();
	void setColor(ofColor color);

	void setCollisionMap(std::vector<std::vector<bool>>* colmap);


protected:
	void updateControls();
	bool checkCollisionWithMap();
	void move();
	bool mpAlive;

	C_Rectangle   mpRect;
	ofColor mpColor;	//Color

	int mpDirection;	//Direcci�n
	int mpSpeed;		//Velocidad
	int mpXtoGo;
	int mpYtoGo;
	bool mpMoving;

	C_Rectangle		mpGraphicRect;
	ofImage*		mpGraphicImg;

	int				mFrame;
	int				mCurrentFrameTime;

	std::vector<std::vector<bool>>* mpColMap;
};

#endif