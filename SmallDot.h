#ifndef SmallDot_H
#define SmallDot_H

#include "includes.h"
#include "ofMain.h"

class SmallDot
{
	public:
		SmallDot();
		SmallDot(int x, int y,int h, int w);
		~SmallDot();

		void update();
		void render();
		void init();

		//Setters and getters
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		bool getAlive();
		void setAlive(bool alive);


		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();


		void setColor(ofColor color);
			

	protected:
		int				mpPoints;		//Los puntos	
		bool			mpAlive;		//Miramos si los puntos han sido cogidos o no
	
	private:
		C_Rectangle		mpRect;	   //Atributo rectangulo de la pala
		C_Rectangle		mpGraphicRect;
		ofImage*		mpGraphicImg;

		int				mFrame;
		int				mCurrentFrameTime;

		ofColor			mpColor;    //Color de la pala

};

#endif