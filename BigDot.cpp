//Include our classes
#include "Scene.h"
#include "BigDot.h"

BigDot::BigDot() {
	mpAlive = true;
	setXY(0, 0);
	setW(TILE_SIZE / 2);
	setH(TILE_SIZE / 2);		//Inicializar en alg�n sitio
	
	mpColor = ofColor(0, 0, 0);

	mpGraphicImg = new ofImage();
	mpGraphicImg->load("BigDot.png");

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mFrame = 0;
	mCurrentFrameTime = 0;
}

BigDot::BigDot(int x, int y, int h, int w) :BigDot(){
	setXY(x, y);
	setW(w);
	setH(h);
}

void BigDot::init() {
	mpAlive = true;
}

bool BigDot::getAlive() { 
	return mpAlive; 
}
void BigDot::setAlive(bool alive) {
	mpAlive = alive;
}


BigDot::~BigDot() {//Solo tiene que tener cosas si hay atributos que cambien de tama�o, los atributos constantes no.

}

void BigDot::update() {
	if (!mpAlive) {return;}
}


void BigDot::render() {
	if (!mpAlive) { return; }
	ofSetColor(255, 255, 255);
	mpGraphicImg->drawSubsection(mpRect.x - 7, mpRect.y - 7,
								 mpGraphicRect.w, mpGraphicRect.h,
								 mpGraphicRect.x, mpGraphicRect.y);
}


//Setters and getters
void BigDot::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setW(a_rect.h);
	return;
}

void BigDot::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void BigDot::setX(int x) {
	mpRect.x = x;
	return;
}

void BigDot::setY(int y) {
	mpRect.y = y;
	return;
}
void BigDot::setW(int w) {
	mpRect.w = w;
	return;
}

void BigDot::setH(int h) {
	mpRect.h = h;
	return;
}


//Gets
C_Rectangle BigDot::getRect() {

	return mpRect;
}
int BigDot::getX() {

	return mpRect.x;
}
int BigDot::getY() {

	return mpRect.y;
}
int BigDot::getW() {

	return mpRect.w;
}
int BigDot::getH() {

	return mpRect.h;
}

void BigDot::setColor(ofColor color){
	mpColor = color;
	return;
}