#include <fstream>
#include <sstream>
#include <iostream>
#include "SmallDot.h"
#include "BigDot.h"
#include "Ghost.h"
#include "Scene.h"
#include "Score.h"

Scene::Scene() {

}

Scene::~Scene() {

}

void Scene::init() {
	mpPlayer = new Pacman();
	mpPlayer->init();

	mpScore = new Score(800, 100, 0, 0);


	//sounds init
	mySound = new ofSoundPlayer();
	mySound->loadSound("pacman_beginning.wav");

	mySound2 = new ofSoundPlayer();
	mySound2->loadSound("pacman_chomp.wav");

	mySound3 = new ofSoundPlayer();
	mySound3->loadSound("pacman_beginning.wav");


	
	initMap();

	mpPlayer->setCollisionMap(&mpCollisionMap);

	mpOffsetX = 0;
	mpOffsetY = 0;
	mpDotsToEat = 0;

	mpScore->setValue(0);

	mySound->play();

}

void Scene::update() {

	mpPlayer->update();

	C_Rectangle playerRect = mpPlayer->getRect();

	int size = 0;

	//Check collisions between entities
	size = mpSmallDots.size();
	for (int i = 0; i < size; i++) {
		SmallDot* smDot = mpSmallDots[i];
		smDot->update();
		if (smDot->getAlive()) {		//Inicializar		
			if (C_RectangleCollision(playerRect, smDot->getRect())) {
				smDot->setAlive(false);	//Inicializar	
				mpDotsEaten++;
				mySound2->play();
				mpScore->addValue(10);
			}
		}
	}

	size = mpBigDots.size();
	for (int i = 0; i < size; i++) {
		BigDot* bigDot = mpBigDots[i];
		bigDot->update();
		if (bigDot->getAlive()) {		//Inicializar		
			if (C_RectangleCollision(playerRect, bigDot->getRect())) {
				bigDot->setAlive(false);	//Inicializar	
				mpDotsEaten++;
				mySound2->play();
				mpScore->addValue(100);
			}
		}
	}


	size = mpGhost.size();
	for (int i = 0; i < size; i++) {
		Ghost* aGhost = mpGhost[i];
		aGhost->update();
		if (aGhost->getAlive()) {
			if (C_RectangleCollision(playerRect, aGhost->getRect())) {
				aGhost->setAlive(false);
				mpScore->addValue(500);
			}
		}
	}


	return;
}

void Scene::render() {

	renderMap();

	int size = mpSmallDots.size();
	for (int i = 0; i < size; i++) {
		mpSmallDots[i]->render();
	}

	size = mpBigDots.size();
	for (int i = 0; i < size; i++) {
		mpBigDots[i]->render();
	}


	size = mpGhost.size();
	for (int i = 0; i < size; i++) {
		mpGhost[i]->render();
	}

	ofSetColor(0, 255, 0);

	mpScore->render();

	

	mpPlayer->render();

}

void Scene::initMap() {
	std::fstream file;
	std::string line;
	int h, w;

	file.open("Map.txt", std::ios::in);
	if (!file.is_open()) {
		std::cout << "Error" << std::endl;
		system("pause");
		exit(0);
	}
	std::getline(file, line);
	w = stoi(line);
	std::getline(file, line);
	h = stoi(line);

	mpCollisionMap.resize(h);
	for (int i = 0; i < h; i++) {
		mpCollisionMap[i].resize(w);
		std::getline(file, line);
		for (int j = 0; j < w; j++) {
			char c = line[j];
			mpCollisionMap[i][j] = false;
			switch (c) {
			case '#':		//Wall
				mpCollisionMap[i][j] = true;
				break;

			case 'P':		//Pacman
				mpPlayer->setXY(j*TILE_SIZE, i*TILE_SIZE);
				break;

			case 'G':		//Ghost
			{
				Ghost* aGhost = new Ghost();
				aGhost->setXY(j*TILE_SIZE + (TILE_SIZE - aGhost->getW()) / 2,
					i*TILE_SIZE + (TILE_SIZE - aGhost->getH()) / 2);
				aGhost->setCollisionMap(&mpCollisionMap);
				mpGhost.push_back(aGhost);
				break;
			}

			case '.':		//Small Dot
			{
				SmallDot* aDot = new SmallDot();
				aDot->setXY(j*TILE_SIZE + (TILE_SIZE - aDot->getW()) / 2,
					i*TILE_SIZE + (TILE_SIZE - aDot->getH()) / 2);
				mpSmallDots.push_back(aDot);
				mpDotsToEat++;
				break;
			}

			case 'O':		//Big Dot
			{
				BigDot* aBigDot = new BigDot();
				aBigDot->setXY(j*TILE_SIZE + (TILE_SIZE - aBigDot->getW()) / 2,
					i*TILE_SIZE + (TILE_SIZE - aBigDot->getH()) / 2);
				mpBigDots.push_back(aBigDot);
				mpDotsToEat++;
				break;
			}
			}
		}
	}
	file.close();
	return;
}

void Scene::renderMap() {

	ofSetColor(27, 0, 253);
	int sizeV = mpCollisionMap.size();
	int sizeH = 0;
	if (sizeV > 0) {
		sizeH = mpCollisionMap[0].size();
	}
	else {
		return;
	}

	for (int j = 0; j < sizeV; j++) {
		for (int i = 0; i < sizeH; i++) {
			if (mpCollisionMap[j][i]) {
				ofDrawRectangle(i*TILE_SIZE + mpOffsetX,
					j*TILE_SIZE + mpOffsetY,
					TILE_SIZE, TILE_SIZE);
			}
		}
	}
}