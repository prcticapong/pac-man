#pragma once

#ifndef SCENE_H
#define SCENE_H

#include "ofMain.h"
#include "includes.h"
#include "Utils.h"
#include "Pacman.h"
#include "SmallDot.h"
#include "BigDot.h"
#include "Ghost.h"
#include "Score.h"

#define TILE_SIZE 32

class Scene

{
public:
	Scene();
	~Scene();

	void init();
	void update();
	void render();

	bool isOfClass(std::string classType);

	std::string getClassName() { return "Scene"; };

protected:
	void initMap();
	void renderMap();

	//Mover el mapa cuando lo vayamos a pintar
	int mpOffsetX;
	int mpOffsetY;
	int lives;

	std::vector<std::vector<bool>> mpCollisionMap;

	Pacman* mpPlayer;
	Score* mpScore;
	ofTrueTypeFont* mpFont;
	ofSoundPlayer* mySound;
	ofSoundPlayer* mySound2;
	ofSoundPlayer* mySound3;
	ofSoundPlayer* mySound4;

	std::vector<SmallDot*> mpSmallDots;
	std::vector<BigDot*> mpBigDots;
	std::vector<Ghost*> mpGhost;

	int				mpDotsToEat;
	int				mpDotsEaten;

	C_Rectangle		mpGraphicRect;
	ofImage*		mpGraphicImg;

	int				mFrame;
	int				mCurrentFrameTime;

};


#endif