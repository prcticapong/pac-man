//Include our classes
#include "Scene.h"
#include "Ghost.h"

Ghost::Ghost() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;
	mpSpeed = 200;
	mpColor = ofColor(0, 0, 0);
	mpDirection = NONE;
	mpMoving = false;
	mpXtoGo = 0;
	mpYtoGo = 0;

	mpGraphicImg = new ofImage();
	mpGraphicImg->load("Ghost.png");

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mFrame = 0;
	mCurrentFrameTime = 0;
	
}

Ghost::Ghost(int x, int y, int w, int h) {
	Ghost();
	setXY(x, y);
	setW(w);
	setH(h);
	mpXtoGo = x;
	mpYtoGo = y;
}


Ghost::~Ghost() {

}


void Ghost::init() {

}


void Ghost::update() {
	updateControls();
	if (!checkCollisionWithMap()) {		//checkCollisionWithMap() == false
		move();
	}else {
		//updateControls();
		mpMoving = false;
	}
	return;
}

bool Ghost::getAlive() {
	return mpAlive;
}

void Ghost::setAlive(bool alive) {
	mpAlive = alive;
}


void Ghost::updateControls() {
if (!mpMoving) {
	int move_enemy = rand() % 4;
		if (move_enemy == 0) {
			mpXtoGo = mpRect.x;
			mpYtoGo = mpRect.y - TILE_SIZE;
			mpMoving = true;
		}else
		if (move_enemy == 1) {
			mpXtoGo = mpRect.x - TILE_SIZE;
			mpYtoGo = mpRect.y;
			mpMoving = true;
		}else
		if (move_enemy == 2) {
			mpXtoGo = mpRect.x;
			mpYtoGo = mpRect.y + TILE_SIZE;
			mpMoving = true;
		}else
		if (move_enemy == 3) {
			mpXtoGo = mpRect.x + TILE_SIZE;
			mpYtoGo = mpRect.y;
			mpMoving = true;
		}
	}
	return;

}

void Ghost::move() {
	if (mpMoving) {
		int y_aux = mpRect.y;
		int x_aux = mpRect.x;
		if (mpRect.x < mpXtoGo) {
			mpRect.x += mpSpeed*global_delta_time/1000;
		}
		else if (mpRect.x > mpXtoGo) {
			mpRect.x -= mpSpeed*global_delta_time / 1000;
		}
		if (mpRect.y < mpYtoGo) {
			mpRect.y += mpSpeed*global_delta_time / 1000;
		}
		else if (mpRect.y > mpYtoGo) {
			mpRect.y -= mpSpeed*global_delta_time / 1000;
		}
		if ((x_aux > mpXtoGo && mpRect.x < mpXtoGo) ||
			(x_aux < mpXtoGo && mpRect.x > mpXtoGo)) {
			mpRect.x = mpXtoGo;
		}
		if ((y_aux > mpYtoGo && mpRect.y < mpYtoGo) ||
			(y_aux < mpYtoGo && mpRect.y > mpYtoGo)) {
			mpRect.y = mpYtoGo;
		}
		if (mpRect.x == mpXtoGo && mpRect.y == mpYtoGo) {
			mpMoving = false;
		}
	}
	return;
}

void Ghost::render() {
	ofSetColor(255, 255, 255);
	mpGraphicImg->drawSubsection(mpRect.x, mpRect.y,
								 mpGraphicRect.w, mpGraphicRect.h,
								 mpGraphicRect.x, mpGraphicRect.y);
	return;
}

bool Ghost::checkCollisionWithMap() {
	//if (mpDirection == NONE) { return  false; }
	int checkX = mpXtoGo / TILE_SIZE;
	int checkY = mpYtoGo / TILE_SIZE;
	
	if (checkY < 0 || checkY >= (*mpColMap).size()) {
		return true;
	}

	if (checkX < 0) {
		mpRect.x = ((*mpColMap)[0].size() - 1)*TILE_SIZE;
		mpXtoGo = mpRect.x;
		return false;
	}
	if (checkX >= (*mpColMap)[0].size()) {
		mpRect.x = 0;
		mpXtoGo = mpRect.x;
		return false;
	}
	return (*mpColMap)[checkY][checkX];
}
	//Bordes de pantalla


//Setters and getters
void Ghost::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}
void Ghost::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Ghost::setX(int x) {
	mpRect.x = x;
	mpXtoGo = x;
	return;
}
void Ghost::setY(int y) {
	mpRect.y = y;
	mpYtoGo = y;
	return;
}
void Ghost::setW(int w) {
	mpRect.w = w;
	return;
}
void Ghost::setH(int h) {
	mpRect.h = h;
	return;
}



C_Rectangle Ghost::getRect() {
	return mpRect;
}
int Ghost::getX() {
	return mpRect.x;
}
int Ghost::getY() {
	return mpRect.y;
}
int Ghost::getW() {
	return mpRect.w;
}
int Ghost::getH() {
	return mpRect.h;
}


void Ghost::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}

int Ghost::getSpeed() {
	return mpSpeed;
}

void Ghost::setColor(ofColor color) {
	mpColor = color;
	return;
}

void Ghost::setCollisionMap(std::vector<std::vector<bool>>* colmap) {
	mpColMap = colmap;
	return;
}