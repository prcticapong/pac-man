//Include our classes
#include "Score.h"
#include "Utils.h"
#include "Scene.h"

Score::Score() {
	mpColor = ofColor(254, 0, 0);
	mpFont = new ofTrueTypeFont();
	mpFont->loadFont("coders_crux.ttf", 65);	//Tipo de letra y tama�o
	number = 0;
}

Score::Score(int x, int y, int h, int w) :Score() {
	setXY(x, y);
	setW(w);
	setH(h);
}

Score::~Score() {
	//Solo tiene que tener cosas si hay atributos que cambien de tama�o, los atributos constantes no.
}

void Score::update() {
	if (mpRect.y < 0)
	{
		mpRect.y = 0;
	}
	if (mpRect.y + mpRect.h > SCREEN_HEIGHT)
	{
		mpRect.y = SCREEN_HEIGHT - mpRect.h;
	}
	return;
}

void Score::render() {
	ofSetColor(0, 255, 0);
	mpFont->drawString("SCORE:", 700, 50);
	ofSetColor(255, 255, 255);
	mpFont->drawString(itos(number, 5), mpRect.x, mpRect.y);
	return;
}

void Score::setValue(int num) {
	number = num;
	return;
}

void Score::addValue(int num) {
	number += num;
	return;
}


//Setters and getters
void Score::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}
void Score::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Score::setX(int x) {
	mpRect.x = x;
	return;
}
void Score::setY(int y) {
	mpRect.y = y;
	return;
}
void Score::setW(int w) {
	mpRect.w = w;
	return;
}
void Score::setH(int h) {
	mpRect.h = h;
	return;
}

//Gets
C_Rectangle Score::getRect() {
	return mpRect;
}
int Score::getX() {
	return mpRect.x;
}
int Score::getY() {
	return mpRect.y;
}
int Score::getW() {
	return mpRect.w;
}
int Score::getH() {
	return mpRect.h;
}

void Score::setSpeed(int speed) {

}

void Score::setColor(ofColor color){
	mpColor = color;
	return;
}