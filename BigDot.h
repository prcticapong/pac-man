#ifndef BigDot_H
#define BigDot_H

#include "includes.h"
#include "ofMain.h"

class BigDot
{
	public:
		BigDot();
		BigDot(int x, int y,int h, int w);
		~BigDot();

		void update();
		void render();
		void init();

		//Setters and getters
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		bool getAlive();
		void setAlive(bool alive);


		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();


		void setColor(ofColor color);
			

	protected:
		int mpBigPoints;		//Los puntos	
		bool mpAlive;		//Miramos si los puntos han sido cogidos o no
	
	private:
		C_Rectangle		mpRect;	   //Atributo rectangulo de la pala
		C_Rectangle		mpGraphicRect;
		ofImage*		mpGraphicImg;

		int				mFrame;
		int				mCurrentFrameTime;

		ofColor			mpColor;    //Color de la pala
};

#endif