//Include our classes
#include "Scene.h"
#include "Pacman.h"

Pacman::Pacman() {
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = TILE_SIZE;
	mpRect.h = TILE_SIZE;

	mpSpeed = 200;

	mpColor = ofColor(255, 255, 255);

	mpDirection = NONE;

	mpMoving = false;

	mpXtoGo = 0;
	mpYtoGo = 0;

	

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = TILE_SIZE;
	mpGraphicRect.h = TILE_SIZE;

	mpGraphicImg = new ofImage();
	mpGraphicImg->load("Paquirrin.png");

	mFrame = 0;
	mCurrentFrameTime = 0;
	
}

Pacman::Pacman(int x, int y, int w, int h) {
	Pacman();
	setXY(x, y);
	setW(w);
	setH(h);
	mpXtoGo = x;
	mpYtoGo = y;
}


Pacman::~Pacman() {

}


void Pacman::init() {

}


void Pacman::update() {
	updateControls();
	if (!checkCollisionWithMap()) {		//checkCollisionWithMap() == false
		move();
	}else {
		mpMoving = false;
	}
	updateGraphic();
	return;
}

void Pacman::updateGraphic() {
	if (mpDirection != NONE) {
		mCurrentFrameTime += global_delta_time;
		if (mCurrentFrameTime > 80) {
			mCurrentFrameTime = 0;
			mFrame++;
			if (mFrame >= 4) {
				mFrame = 0;
			}
		}
	}
	else {
		mFrame = 0;
		mCurrentFrameTime = 0;
	}
	int row = mpDirection - 1;
	if (row < 0) { row = 0; }
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
	mpGraphicRect.y = row*mpGraphicRect.h;
}

void Pacman::updateControls() {
	if (!mpMoving) {
		if (key_down['W'] || key_down['w']) {
			mpXtoGo = mpRect.x;
			mpYtoGo = mpRect.y - TILE_SIZE;
			mpMoving = true;
			mpDirection = UP;
		}else
		if (key_down['A'] || key_down['a']) {
			mpXtoGo = mpRect.x - TILE_SIZE;
			mpYtoGo = mpRect.y;
			mpMoving = true;
			mpDirection = LEFT;
		}else
		if (key_down['S'] || key_down['s']) {
			mpXtoGo = mpRect.x;
			mpYtoGo = mpRect.y + TILE_SIZE;
			mpMoving = true;
			mpDirection = DOWN;
		}else
		if (key_down['D'] || key_down['d']) {
			mpXtoGo = mpRect.x + TILE_SIZE;
			mpYtoGo = mpRect.y;
			mpMoving = true;
			mpDirection = RIGHT;
		}
		else {
			mpMoving = false;
			mpDirection = NONE;
		}
	}
	return;
}

void Pacman::move() {
	if (mpMoving) {
		int y_aux = mpRect.y;
		int x_aux = mpRect.x;
		if (mpRect.x < mpXtoGo) {
			mpRect.x += mpSpeed*global_delta_time/1000;
		}
		else if (mpRect.x > mpXtoGo) {
			mpRect.x -= mpSpeed*global_delta_time / 1000;
		}
		if (mpRect.y < mpYtoGo) {
			mpRect.y += mpSpeed*global_delta_time / 1000;
		}
		else if (mpRect.y > mpYtoGo) {
			mpRect.y -= mpSpeed*global_delta_time / 1000;
		}
		if ((x_aux > mpXtoGo && mpRect.x < mpXtoGo) ||
			(x_aux < mpXtoGo && mpRect.x > mpXtoGo)) {
			mpRect.x = mpXtoGo;
		}
		if ((y_aux > mpYtoGo && mpRect.y < mpYtoGo) ||
			(y_aux < mpYtoGo && mpRect.y > mpYtoGo)) {
			mpRect.y = mpYtoGo;
		}
		if (mpRect.x == mpXtoGo && mpRect.y == mpYtoGo) {
			mpMoving = false;
		}
	}
	return;
}

void Pacman::render() {
	ofSetColor(255, 255, 0);
	mpGraphicImg->drawSubsection(mpRect.x, mpRect.y,
								 mpGraphicRect.w, mpGraphicRect.h,
								 mpGraphicRect.x, mpGraphicRect.y);
	return;
}

bool Pacman::checkCollisionWithMap() {
	//if (mpDirection == NONE) { return  false; }
	int checkX = mpXtoGo / TILE_SIZE;
	int checkY = mpYtoGo / TILE_SIZE;
	
	if (checkY < 0 || checkY >= (*mpColMap).size()) {
		return true;
	}

	if (checkX < 0) {
		mpRect.x = ((*mpColMap)[0].size() - 1)*TILE_SIZE;
		mpXtoGo = mpRect.x;
		return false;
	}
	if (checkX >= (*mpColMap)[0].size()) {
		mpRect.x = 0;
		mpXtoGo = mpRect.x;
		return false;
	}
	return (*mpColMap)[checkY][checkX];
}
	//Bordes de pantalla




//Setters and getters
void Pacman::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}
void Pacman::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}
void Pacman::setX(int x) {
	mpRect.x = x;
	mpXtoGo = x;
	return;
}
void Pacman::setY(int y) {
	mpRect.y = y;
	mpYtoGo = y;
	return;
}
void Pacman::setW(int w) {
	mpRect.w = w;
	return;
}
void Pacman::setH(int h) {
	mpRect.h = h;
	return;
}



C_Rectangle Pacman::getRect() {
	return mpRect;
}
int Pacman::getX() {
	return mpRect.x;
}
int Pacman::getY() {
	return mpRect.y;
}
int Pacman::getW() {
	return mpRect.w;
}
int Pacman::getH() {
	return mpRect.h;
}



void Pacman::setSpeed(int speed) {
	mpSpeed = speed;
	return;
}

int Pacman::getSpeed() {
	return mpSpeed;
}

void Pacman::setColor(ofColor color) {
	mpColor = color;
	return;
}

void Pacman::setCollisionMap(std::vector<std::vector<bool>>* colmap) {
	mpColMap = colmap;
	return;
}